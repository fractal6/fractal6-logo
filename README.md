# Fractal Logo

Generate fractal images directly from fractal equations.

### Install

    pip install --user -r requirements.txt

### Help

`python3 fractal.py --help`

